# Instalation and Usage Guideline


1.  Install this project by installing the requirements

```
pip install -r requirements.txt
```

2.  Configure the database

```
python3 manage.py makemigrations
python3 manage.py migrate
```

3.  Add sample data to the database

```
python3 loaddata sampledata.json
```

4.  View all available urls via the terminal

```
python3 manage.py show_urls
```

5.  Run the server

```
python manage.py runserver
```


After step 3 you will have an admin account with credentials

```
username: admin
password: asdf1234
```
