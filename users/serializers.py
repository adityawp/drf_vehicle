from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from .models import Users


class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = (
            "username",
            "password",
            "email",
            "is_superuser",
            "first_name",
            "last_name",
            "is_staff",
            "is_active",
            "groups",
            "user_permissions",
        )
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        validated_data["password"] = make_password(validated_data["password"])
        return super().create(validated_data)

    def update(self, instance, validated_data):
        validated_data["password"] = make_password(validated_data["password"])
        instance.set_password(validated_data["password"])
        return super().update(instance, validated_data)
