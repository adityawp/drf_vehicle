from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import PricelistViewSet, VehicleBrandViewSet, VehicleModelViewSet, VehicleTypeViewSet, VehicleYearViewSet

# Create a router and register our viewsets with it.
pricelist = DefaultRouter()
pricelist.register("", PricelistViewSet)
vehicle_brand = DefaultRouter()
vehicle_brand.register("", VehicleBrandViewSet)
vehicle_model = DefaultRouter()
vehicle_model.register("", VehicleModelViewSet)
vehicle_type = DefaultRouter()
vehicle_type.register("", VehicleTypeViewSet)
vehicle_year = DefaultRouter()
vehicle_year.register("", VehicleYearViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path("pricelist/", include(pricelist.urls)),
    path("brand/", include(vehicle_brand.urls)),
    path("model/", include(vehicle_model.urls)),
    path("type/", include(vehicle_type.urls)),
    path("year/", include(vehicle_year.urls)),
]
