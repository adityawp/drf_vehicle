import datetime

from django.core.validators import MinValueValidator
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from djmoney.models.fields import MoneyField


def current_year():
    return datetime.date.today().year


class VehicleBrand(models.Model):
    name = models.CharField(_("Vehicle brand name"), max_length=100, unique=True)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _("Vehicle brand")
        verbose_name_plural = _("Vehicle brands")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("vehicle_brand_detail", kwargs={"pk": self.pk})


class VehicleType(models.Model):
    name = models.CharField(_("Vehicle type"), max_length=100, unique=True)
    brand_id = models.ForeignKey(
        VehicleBrand, verbose_name=_("Vehicle brand"), on_delete=models.SET_NULL, blank=True, null=True
    )
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    unique_together = ["name", "brand_id"]

    class Meta:
        verbose_name = _("Vehicle type")
        verbose_name_plural = _("Vehicle types")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("vehicle_type_detail", kwargs={"pk": self.pk})


class VehicleModel(models.Model):
    name = models.CharField(_("Vehicle model"), max_length=100)
    type_id = models.ForeignKey(
        VehicleType, verbose_name=_("Vehicle type"), on_delete=models.SET_NULL, blank=True, null=True
    )
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    unique_together = ["name", "type_id"]

    class Meta:
        verbose_name = _("Vehicle model")
        verbose_name_plural = _("Vehicle models")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("vehicle_model_detail", kwargs={"pk": self.pk})


class VehicleYear(models.Model):
    year = models.PositiveIntegerField(_("Vehicle year"), validators=[MinValueValidator(1886)], unique=True)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _("Vehicle year")
        verbose_name_plural = _("Vehicle years")

    def __str__(self):
        return str(self.year)

    def get_absolute_url(self):
        return reverse("vehicle_year_detail", kwargs={"pk": self.pk})


class Pricelist(models.Model):
    code = models.CharField(_("Code"), max_length=100, unique=True)
    price = MoneyField(max_digits=15, decimal_places=2, default_currency="IDR")
    year_id = models.ForeignKey(
        VehicleYear, verbose_name=_("Vehicle year"), on_delete=models.SET_NULL, blank=True, null=True
    )
    model_id = models.ForeignKey(
        VehicleModel, verbose_name=_("Vehicle model"), on_delete=models.SET_NULL, blank=True, null=True
    )
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    unique_together = ["price", "year_id", "model_id"]

    class Meta:
        verbose_name = _("Pricelist")
        verbose_name_plural = _("Pricelists")

    def __str__(self):
        return self.code

    def get_absolute_url(self):
        return reverse("pricelist_detail", kwargs={"pk": self.pk})
