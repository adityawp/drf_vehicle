from rest_framework import filters, viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from .models import Pricelist, VehicleBrand, VehicleModel, VehicleType, VehicleYear
from .serializers import (
    PricelistSerializer,
    VehicleBrandSerializer,
    VehicleModelSerializer,
    VehicleTypeSerializer,
    VehicleYearSerializer,
)


class PricelistViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """

    min_range = 1000000000
    max_range = 3000000000
    queryset = Pricelist.objects.filter(price__range=(min_range, max_range))
    serializer_class = PricelistSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == "list" or self.action == "retrieve":
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class VehicleBrandViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """

    queryset = VehicleBrand.objects.all()
    serializer_class = VehicleBrandSerializer
    filter_backends = [filters.OrderingFilter]
    ordering = ["name"]

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == "list" or self.action == "retrieve":
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class VehicleModelViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """

    queryset = VehicleModel.objects.filter(pk=2)
    serializer_class = VehicleModelSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == "list" or self.action == "retrieve":
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class VehicleTypeViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """

    queryset = VehicleType.objects.filter(pk=1)
    serializer_class = VehicleTypeSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == "list" or self.action == "retrieve":
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class VehicleYearViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """

    min_range = 2013
    max_range = 2018
    queryset = VehicleYear.objects.filter(year__range=(min_range, max_range))
    serializer_class = VehicleYearSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == "list" or self.action == "retrieve":
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
