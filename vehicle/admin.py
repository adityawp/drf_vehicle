from django.contrib import admin

from .models import Pricelist, VehicleBrand, VehicleModel, VehicleType, VehicleYear


class VehicleTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "brand_id")


class VehicleModelAdmin(admin.ModelAdmin):
    list_display = ("name", "type_id")


class PricelistAdmin(admin.ModelAdmin):
    list_display = ("code", "price")


admin.site.register(VehicleBrand)
admin.site.register(VehicleType, VehicleTypeAdmin)
admin.site.register(VehicleModel, VehicleModelAdmin)
admin.site.register(VehicleYear)
admin.site.register(Pricelist, PricelistAdmin)
