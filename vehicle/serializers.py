from rest_framework import serializers

from .models import Pricelist, VehicleBrand, VehicleModel, VehicleType, VehicleYear


class PricelistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pricelist
        fields = "__all__"


class VehicleBrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleBrand
        fields = "__all__"


class VehicleModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleModel
        fields = "__all__"


class VehicleTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleType
        fields = "__all__"


class VehicleYearSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleYear
        fields = "__all__"
