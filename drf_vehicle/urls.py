from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api-vehicle/", include("vehicle.urls")),
    path("api-users/", include("users.urls")),
    path("api-auth/", include("rest_framework.urls")),
]
